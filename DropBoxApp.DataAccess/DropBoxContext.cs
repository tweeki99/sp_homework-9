﻿namespace DropBoxApp.DataAccess
{
    using DropBoxApp.Models;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DropBoxContext : DbContext
    {
        public DropBoxContext()
            : base("name=DropBoxContext")
        {
        }

        public virtual DbSet<User> Users { get; set; }
    }
}