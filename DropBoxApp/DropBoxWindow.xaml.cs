﻿using Dropbox.Api;
using Dropbox.Api.Files;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DropBoxApp
{
    /// <summary>
    /// Логика взаимодействия для DropBoxWindow.xaml
    /// </summary>
    public partial class DropBoxWindow : Window
    {
        static string token = "8QFqrVUO2DAAAAAAAAAAC4PiKoSsuQvdJ5qfiUvP5TYEIQokH1UsDtBYh31ZSMiS";

        ObservableCollection<Item> Items = new AsyncObservableCollection<Item>();

        public DropBoxWindow(MainWindow mainWindow)
        {
            InitializeComponent();
            itemsDataGrid.ItemsSource = Items;

            var task = new Task(ShowFiles);
            task.Start();
        }

        private async void  ShowFiles()
        {
            using (var dropBox = new DropboxClient(token))
            {
                var list = await dropBox.Files.ListFolderAsync("/" + ConfigurationManager.AppSettings["AuthorizationUserId"]);
                Items.Clear();
                foreach (var item in list.Entries.Where(i => i.IsFile))
                {
                    Items.Add(new Item { Name = item.Name });
                }
            }
        }

        private void UploadFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();

            using (var dropBox = new DropboxClient(token))
            {
                string file = openFileDialog.FileName;
                string folder = "/" + ConfigurationManager.AppSettings["AuthorizationUserId"];
                string fileName = openFileDialog.SafeFileName;

                using (var memoryStream = new MemoryStream(File.ReadAllBytes(file)))
                {
                    var updated = dropBox.Files.UploadAsync(folder + "/" + fileName, WriteMode.Overwrite.Instance, body: memoryStream);
                    updated.Wait();
                    var myFile = dropBox.Sharing.CreateSharedLinkWithSettingsAsync(folder + "/" + fileName);
                    myFile.Wait();
                }
            }

            var task = new Task(ShowFiles);
            task.Start();
        }

        private void UploadButtonClick(object sender, RoutedEventArgs e)
        {
            UploadFile();
        }
    }
}
