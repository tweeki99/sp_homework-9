﻿using Dropbox.Api;
using Dropbox.Api.Files;
using DropBoxApp.DataAccess;
using DropBoxApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DropBoxApp
{
    /// <summary>
    /// Логика взаимодействия для RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        static string token = "8QFqrVUO2DAAAAAAAAAAC4PiKoSsuQvdJ5qfiUvP5TYEIQokH1UsDtBYh31ZSMiS";

        public RegistrationWindow(MainWindow mainWindow)
        {
            InitializeComponent();
        }

        private void AcceptButtonClick(object sender, RoutedEventArgs e)
        {
            if (loginText.Text.Length > 0 && passwordText.Password.Length > 0)
            {
                using (var context = new DropBoxContext())
                {
                    string hashedPassword = DropBoxApp.Services.CryptoService.HashPassword(passwordText.Password);
                    User user = new User()
                    {
                        Login = loginText.Text,
                        Password = hashedPassword
                    };

                    context.Users.Add(user);
                    context.SaveChanges();

                    CreateUserFolder(user.Id.ToString());

                    MessageBox.Show("Регистрация выполнена");
                    this.DialogResult = true;
                }
            }
            else MessageBox.Show("Заполните все поля");
        }

        static void CreateUserFolder(string folderName)
        {
            using (var dropBox = new DropboxClient(token))
            {
                string file = "DropBox.png";
                string folder = "/" + folderName;
                string fileName = "DropBox.png";

                using (var memoryStream = new MemoryStream(File.ReadAllBytes(file)))
                {
                    var updated = dropBox.Files.UploadAsync(folder + "/" + fileName, WriteMode.Overwrite.Instance, body: memoryStream);
                    updated.Wait();
                    var myFile = dropBox.Sharing.CreateSharedLinkWithSettingsAsync(folder + "/" + fileName);
                    myFile.Wait();
                }
            }
        }
    }
}
